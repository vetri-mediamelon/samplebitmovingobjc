# SampleBitmovingObjC

[![CI Status](https://img.shields.io/travis/vetri/SampleBitmovingObjC.svg?style=flat)](https://travis-ci.org/vetri/SampleBitmovingObjC)
[![Version](https://img.shields.io/cocoapods/v/SampleBitmovingObjC.svg?style=flat)](https://cocoapods.org/pods/SampleBitmovingObjC)
[![License](https://img.shields.io/cocoapods/l/SampleBitmovingObjC.svg?style=flat)](https://cocoapods.org/pods/SampleBitmovingObjC)
[![Platform](https://img.shields.io/cocoapods/p/SampleBitmovingObjC.svg?style=flat)](https://cocoapods.org/pods/SampleBitmovingObjC)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SampleBitmovingObjC is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'SampleBitmovingObjC'
```

## Author

vetri, vetri@mediamelon.com

## License

SampleBitmovingObjC is available under the MIT license. See the LICENSE file for more info.
