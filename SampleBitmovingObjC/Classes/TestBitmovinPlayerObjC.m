//
//  TestBitmovinPlayerObjC.m
//  Pods-SampleBitmovingObjC_Tests
//
//  Created by MacAir 1 on 18/09/20.
//

#import "TestBitmovinPlayerObjC.h"
#import <BitmovinPlayer/BitmovinPlayer.h>

@interface TestBitmovinPlayerObjC() <BMPPlayerListener>
@property (nonatomic, strong) BMPBitmovinPlayer *player;

@end

@implementation TestBitmovinPlayerObjC

-(void)setBitmovinPlayerForIntegration:(id)player {
    NSLog(@"Hola!!!, Successfully Integrated");
    self.player = (BMPBitmovinPlayer *) player;
    [player addPlayerListener:self];
}

- (void)onSourceLoaded:(BMPSourceLoadedEvent *)event {
    NSLog(@"OnSource loaded called , %@", [event sourceItem]);
    NSLog(@"OnSource loaded called , %@", [event streamType]);
}

- (void)onPlay:(BMPPlayEvent *)event {
    NSLog(@"onPlay: %f", event.time);
}

- (void)onPaused:(BMPPausedEvent *)event {
    NSLog(@"onPaused: %f", event.time);
}

- (void)onTimeChanged:(BMPTimeChangedEvent *)event {
    NSLog(@"onTimeChanged: %f", event.currentTime);
}

- (void)onDurationChanged:(BMPDurationChangedEvent *)event {
    NSLog(@"onDurationChanged: %f", event.duration);
}

- (void)onError:(BMPErrorEvent *)event {
    NSLog(@"onError: %@", event.message);
}

@end
