//
//  TestBitmovinPlayerObjC.h
//  Pods-SampleBitmovingObjC_Tests
//
//  Created by MacAir 1 on 18/09/20.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/BitmovinPlayer.h>

NS_ASSUME_NONNULL_BEGIN

@interface TestBitmovinPlayerObjC : NSObject
-(void) setBitmovinPlayerForIntegration:(BMPBitmovinPlayer *)player;
@end

NS_ASSUME_NONNULL_END
