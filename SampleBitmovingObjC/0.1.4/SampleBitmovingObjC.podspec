#
# Be sure to run `pod lib lint SampleBitmovingObjC.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'SampleBitmovingObjC'
  s.version          = '0.1.4'
  s.summary          = 'A Sample Objc Library to test Bitmovin Player.'
  s.description      = 'A Sample Objc Library to test Bitmovin Player for test'
  s.homepage         = 'https://bitbucket.org/vetri-mediamelon/samplebitmovingobjc'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'vetri' => 'vetri@mediamelon.com' }
  s.source           = { :git => 'https://vetri-mediamelon@bitbucket.org/vetri-mediamelon/samplebitmovingobjc.git', :tag => '0.1.4' }
  s.ios.deployment_target = '8.0'
  s.source_files = 'SampleBitmovingObjC/Classes/**/*', 'SampleBitmovingObjC/Frameworks/BitmovinPlayer.framework/Headers/*.h'
  s.public_header_files = 'SampleBitmovingObjC/Classes/**/*.h'
  s.vendored_frameworks = 'SampleBitmovingObjC/Frameworks/BitmovinPlayer.framework'
  s.static_framework = true
  
  # s.resource_bundles = {
  #   'SampleBitmovingObjC' => ['SampleBitmovingObjC/Assets/*.png']
  # }

  
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
